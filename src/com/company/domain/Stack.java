package com.company.domain;

import com.company.domain.Node;

/**
 * Created by Shovon on 12/9/13.
 */
public class Stack<E> {
    private Node tail;

    public void push(E data) {
        Node node = new Node(data);
        node.setNext(tail);
        tail = node;

    }

    public E pop() {
        E data = (E) tail.getData();
        tail = tail.getNext();
        return data;
    }
}
