package com.company.domain;

import java.util.Stack;

/**
 * Created by Shovon on 12/9/13.
 */
public class MyQueueUsingStack {
    private Stack<Integer> popStack = new Stack<Integer>();
    private Stack<Integer> pushStack = new Stack<Integer>();

    public void enQueue(int item) {
        pushStack.push(item);
    }

    public int deQueue() {
        if (popStack.isEmpty()) {
            while (!pushStack.isEmpty()) {
                popStack.push(pushStack.pop());
            }
        }

        return popStack.pop();
    }
}
