package com.company.domain;

/**
 * Created by Shovon on 12/9/13.
 */
public class Queue<E> {
    private Node head;
    private Node tail;

    public void push(E data) {
        Node node = new Node(data);
        if (head == null) {
            head = node;
            tail = node;
        }
        tail.setNext(node);
        tail = node;

    }

    public E pop() {
        E data = (E) head.getData();
        head = head.getNext();
        return data;
    }
}
