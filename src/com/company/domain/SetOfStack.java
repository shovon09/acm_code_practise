package com.company.domain;

import java.util.ArrayList;

/**
 * Created by Shovon on 12/10/13.
 */
public class SetOfStack<E> {
    private static final int STACK_LIMIT = 5;
    private ArrayList<ArrayList<E>> setOfStacks = new ArrayList<ArrayList<E>>();
    private ArrayList<E> currentStack;
    int head = 0;
    int counter = 0;

    public void push(E item) {
        if (counter == 0) {
            currentStack = new ArrayList<E>(STACK_LIMIT);
            setOfStacks.add(currentStack);
        }
        currentStack.add(item);
        counter++;
        head++;
        if (counter == 5) {
            counter = counter % 5;
        }
    }

    public E pop() {

        int topStack;
        if (head % 5 == 0) {
            setOfStacks.remove(currentStack);
            currentStack = setOfStacks.get((head / 5) - 1);
            topStack = (head-1) % 5;
        } else {
            topStack = (head % 5) - 1;
        }

        E item = currentStack.get(topStack);
        head--;
        return item;

    }

}
