package com.company.domain;

/**
 * Created by Shovon on 12/9/13.
 */
public class Node<E> {
    private E data;

    private Node next;

    public Node(E data) {
        this.data = data;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public E getData() {
        return data;
    }
}
